import React, { Component } from 'react';
import {
  View,
  Text,
 Image
} from 'react-native';
import FitImage from 'react-native-fit-image';
import styles from '../../styles'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
export default class App extends Component{
  render(){
    return(
      <View style={{flex:1,backgroundColor:'#F4F6F7',backgroundColor:'#fff'}}>
        <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
          <View style = {{justifyContent:'center',alignItems:'center',marginTop:hp('3%')}}>
          <Text style={{fontSize:wp('5%'),color:'#17CAE8',fontWeight:'bold'}}>Emergency Number</Text>
          </View>

  <View style = {{flexDirection:'row',marginLeft:wp('5%'),marginTop:hp('2%'),marginRight:wp('2%')}}>
    <Icon name='cards-diamond' size={15} color='#17CAE8'/>
    <Text style={{marginLeft:wp('2%'),color:'red',fontWeight:'bold',fontSize:hp('2%')}}>The Helpline Number for corona-virus : +91-11-23978046</Text>
    </View> 
  


    <View style = {{flexDirection:'row',marginLeft:wp('5%'),marginTop:hp('2%'),marginRight:wp('2%')}}>
    <Icon name='cards-diamond' size={15} color='#17CAE8'/>
    <Text style={{marginLeft:wp('2%'),color:'red',fontWeight:'bold',fontSize:hp('2%')}}>The Helpline Number of State & Union Territories for corona-virus</Text>
    </View> 
    
    <View style = {{flexDirection:'row',marginLeft:wp('5%'),marginTop:hp('2%'),marginRight:wp('2%')}}>
    <Icon name='cards-diamond' size={15} color='#17CAE8'/>
    <Text style={{marginLeft:wp('2%'),color:'red',fontWeight:'bold',fontSize:hp('2%')}}>The Helpline Email ID for corona-virus:ncov2019(at)gmail[dot]com</Text>
    </View> 

<Image source={require('../../../assets/India.jpeg')} style={{width:wp('108%'),height:hp('100%'),marginTop:hp('2%')}}/>
</ScrollView>
      </View>

      )
  }
}
