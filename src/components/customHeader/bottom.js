import { Platform, StyleSheet, View, Text, Image } from 'react-native';
import React, { Component } from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default class BottomView extends React.Component {
    render() {
        return (

            <View style={styles.MainContainer}>
                <Text style={styles.textStyle}>Powered by </Text>
                <Image source={require('../../../assets/cureAssit_logo-1.png')}></Image>
                <Text style={styles.textStyle}>ureassist </Text>
            </View>

        );
    }
}

const styles = StyleSheet.create(
    {
        MainContainer:
        {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            flexDirection:'row',
            backgroundColor: '#335e90',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: 0,
            padding:hp('1%')
        },



        textStyle: {

            color: '#fff',
            fontSize: hp('1.5%'),
            fontFamily:'sans-serif-condensed'
        }
    });