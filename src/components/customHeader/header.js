import * as React from 'react';
import {Alert} from 'react-native'
import { Appbar,Avatar } from 'react-native-paper';
import Colors from '../colors'
import styles from '../../styles'
export default class MyComponent extends React.Component {
  _goBack = () => console.log('Went back');

  _handleSearch = () => console.log('Searching');

  _handleMore = () =>Alert.alert('clicked')

  render() {
    return (
      <Appbar.Header style={styles.headder}>
         <Avatar.Image size={38} source={require('../../../assets/cvid.png')} />
        <Appbar.Content
          title="Corona Tracker"
        
        />
       
        <Appbar.Action icon="account" onPress={this._handleMore} />
      </Appbar.Header>
    );
  }
}