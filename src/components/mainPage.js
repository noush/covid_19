import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    Alert,
    Image
} from 'react-native';
import styles from '../styles'
import { ListItem, Avatar } from 'react-native-elements'
import Header from '../components/customHeader/header'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {Actions} from 'react-native-router-flux'
import Colors from './colors'
export default class App extends Component {
    constructor() {
        super();
        this.state = {
            shouldDoList: [{
                text: 'Assess Yourself',
                imageUrl: require('../../assets/covid19.jpg')
            },
            {
                text: 'Add Places Visited and People met in last 14 days',
                imageUrl: require('../../assets/map.png')
            },
            {
                text: 'Coronavirus disease COVID-19 situation dashboard',
                imageUrl: require('../../assets/Coronavirus5TrackingTools_1024.jpg')
            },
            {
                text: 'Emergency Contact',
                imageUrl: require('../../assets/contactLogo.png')
            }]
        }
    }


    actionOnRow(item, index) {
        
        //Alert.alert(index.toString());
        let rowIndexValue = index.toString()
        switch (rowIndexValue) {
            case '0': {Actions.assessYourself()}
                break;
            case '1': //Linking.openURL(URL).catch((err) => console.error('An error occurred', err));
            Actions.test()
                break;
            case '2':  Actions.testMap()
                break;
            case '3':// Actions.contact()
            Actions.contactTracing()
                break;
          
            default:
            
        }
    }
    

    renderItem = ({ item , index}) => (
        <TouchableOpacity style={{
            marginLeft: wp('2%'), marginTop: wp('2%'), marginBottom: wp('2%'), flex:1,
            borderRadius: wp('5%'), elevation: 2, shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.1,
            flexDirection: 'row',alignItems:'center',backgroundColor:Colors.innerFlex }} 
            onPress = {() =>this.actionOnRow(item, index)}>
            
       
           
                <View style={{ alignSelf: 'flex-start',justifyContent:'center',}}>
                <Image source={item.imageUrl} style ={{width:wp('25%'),height:hp('15%'),marginLeft:wp('5%'),marginTop:hp('2%'),borderRadius:wp('5%'),marginBottom:hp('2%')}} />
            </View>
            <View style={{ marginLeft:wp('10%'),flex: 1}}>
                <Text style={{ justifyContent: 'center', alignItems: 'center',marginRight:wp('5%'),fontSize:wp('4%'),fontWeight:'bold',color:Colors.text}}>{item.text}</Text>
            </View>
           

        </TouchableOpacity>
        //       <TouchableOpacity
        //       onPress = {() =>{Alert.alert('clicked')}}>
        //     <ListItem
        //     containerStyle =  {{marginLeft:wp('2%'),marginTop:wp('2%'),marginBottom:wp('2%'),width:wp('92%'),height:hp('18%'),
        //     borderRadius:wp('5%'),elevation:8,shadowOffset: { width: 0, height: 10 },shadowOpacity: 0.7,backgroundColor:'red'}}
        //     title={item.text}
        //     titleStyle={{ color: 'black', fontWeight: 'bold',fontSize:hp('3%'),alignItems:'center'}}
        //     chevron={{ color: 'green' }}
        //   />
        //    </TouchableOpacity>
    )

    keyExtractor = (item, index) => index.toString()


    render() {
        return (

            <View style={styles.mainPage}>
                <Header/>
                <View style={styles.innercontainer}>
                <FlatList
                    data={this.state.shouldDoList}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem} />
                    </View>
            </View>
        )
    }
}
