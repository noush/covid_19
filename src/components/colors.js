const COLORS = {
    theme: '#335e90',
    text: '#fff',
    Head :'#17CAE8',
    innerFlex:'#0478db',
    // your colors
  }
  //Android[normal,notoserif,sans-serif,sans-serif-light,sans-serif-thin,sans-serif-condensed,sans-serif-medium,serif,Roboto,monospace]txt built in family
  export default COLORS;