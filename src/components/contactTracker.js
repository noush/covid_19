import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Alert
} from 'react-native';
import styles from '../styles'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Colors from './colors'
import { Icon } from 'react-native-vector-icons/MaterialCommunityIcons';
export default class App extends Component {

  handlePhoneNumberChange = () => {
    Alert.alert('rej;fk;ofk')
  }


  render() {

    
    return (
      <View style={styles.container}>
        <View >
          <Text style={styles.Heading}>Assess Yourself</Text>
          <Text style={styles.subheading}>Information on places visited and people met in last 14 days</Text>
        </View>
        <View style={{ flex: 1, backgroundColor: Colors.innerFlex }}>
          <Text style={styles.submain}>
            People Met
          </Text>
          <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: hp('2%') }}>
                {/* <Icon style={[{ color: 'blue' }]} size={25} name={'plus-circle-outline'} /> */}
            <View style={{ width: wp('70%') }}>
              <TextInput
                style={styles.txtInput}
                maxLength={20}
                keyboardType={"phone-pad"}
                //value={this.state.name}
                onSubmitEditing={this.handlePhoneNumberChange}
              />
            </View>
             
         
            
          </View>
        </View>
      </View>
    )
  }
}
