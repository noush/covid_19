import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ImageComponent from '../imageAsses/imageComponent'
import styles from '../../styles'
export default class App extends Component{
  render(){
    return(
      <View style={styles.container}>
      <ImageComponent />
      <Text style={{marginTop:hp('5%'),fontSize:hp('2%'),marginLeft:wp('2%')}}>Excellent! Continue to follow precautionary measures as suggested by Health Administrator</Text>
      </View>
      )
  }
}
