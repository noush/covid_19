import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList
} from 'react-native';
import styles from '../../styles'
import {Actions} from 'react-native-router-flux'
import ImageComponent from '../imageAsses/imageComponent'
import CheckBox from '@react-native-community/checkbox';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons'

export default class Assess extends Component {
  constructor() {
    super();
    this.state = {
      checkStatusYes: false,
      checkStatusNo : false,
      shouldDoList: [{
        text: 'About COVID-19'
      },
      {
        text: 'Be Prepared'
      },
      {
        text: 'Protect Yourself And Others'
      },
      {
        text: 'Myth Busters'
      },
      {
        text: 'Stress Busters'
      }]
    }
  }
  renderSeperator = () => {
    return (
      <View style={{ height: 1, width: wp('70%'), backgroundColor: '#E5E7E9' }}>

      </View>
    )
  }
  renderItem = ({ item }) => (
    <View style={{ flexDirection: 'row', width: wp('96%'), height: hp('8%') }}>
      <View style={{  width: wp('70%')}}>
      <Text style={{ color: '#2874A6', fontSize: wp('2.5%'), fontWeight: 'bold', justifyContent: 'center', marginLeft: wp('2%'), marginTop: hp('2%') }}>{item.text}</Text>
      </View>
    <View style={{flexDirection:'row'}}>
     
      <CheckBox
      style={{marginLeft:wp('4%'),marginRight:wp('5%'),marginTop:hp('2%')}}
    value={this.state.checkStatusYes}
    disabled={false}
    onValueChange = {(prevState)=> {
      this.setState({
        checkStatusYes : !prevState
      },
      ()=>{
        Actions.contact()
      }
      )
    }
  }
    // onValueChange ={(prevState)=>{
    //   this.setState({
    //     checkStatusYes : !prevState
    //   })
    // }}
  /> 
      <CheckBox
       style={{marginTop:hp('2%')}}
       value={this.state.checkStatusNo}
    disabled={false}
   // onChange = {()=> Actions.symptomNo()}
  />
    </View>
    

    </View>
  )
 
  keyExtractor = (item, index) => index.toString()
 
  render() {
    return (
      <View style={styles.container}>
        <ImageComponent />
        <View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', width: wp('25%') ,marginTop:hp('2%'),height:hp('4%'),alignSelf:'flex-end'}}>
            <View>
            <Text>YES</Text>
            </View>
            <View>
              <Text>NO</Text>
            </View>

          </View>
          <FlatList
            data={this.state.shouldDoList}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
            ItemSeparatorComponent={this.renderSeperator} />
            
        </View>
      </View>
    )
  }
}
