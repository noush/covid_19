import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Alert
} from 'react-native';
import styles from '../../styles'
import FitImage from 'react-native-fit-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux'
export default class ImageComponent extends Component {
    render() {
        return (
           
            <TouchableOpacity 
            onPress={()=> Actions.assess() } style={styles.imageView}>
            <FitImage
                source={require('../../../assets/home-banner.jpg')}
                style={styles.fitImage}
            />
            <View style={styles.imageTextView}>
                <Text style={styles.risk}>
                    Covid Risk Assestment
                </Text>
                <Text style={styles.assess}>
                    Assess Yourself
                </Text>
            </View>
            </TouchableOpacity>
        )
    }
}

