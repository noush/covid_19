import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  ImageBackground
} from 'react-native';
import { SliderBox } from "react-native-image-slider-box";
import { widthPercentageToDP as wp, heightPercentageToDP  as hp} from 'react-native-responsive-screen';
import FitImage from 'react-native-fit-image';
export default class App extends Component{
    constructor(props) {
        super(props);
        this.state = {
            images: [
                // "https://source.unsplash.com/1024x768/?tree",
                require('../../../../assets/BePrepared/BR1.jpg'),
                require('../../../../assets/BePrepared/BR2.jpg'),
                require('../../../../assets/BePrepared/BR3.jpg')
            ],
            languageCode: 'et',
            translate: false,
            selectedCountry : 'English'
        };
    }

  render(){
    return(
        <View style= {{flex:1}}>
  {/* <ImageBackground source={require('../../../../assets/img1000.jpg')} style={{width: '100%', height: '100%'}}>
  </ImageBackground> */}
  <SliderBox
                        images={this.state.images}
                        sliderBoxHeight={110}
                        ImageComponentStyle={{ borderRadius: 15, width: wp('97%'), marginTop: 5,height:hp('100%') }}
                        onCurrentImagePressed={index =>
                            console.warn(`image ${index} pressed`)
                        }
                        autoplay
                        circleLoop
                        resizeMethod={'resize'}
                        resizeMode={'cover'}
                        dotColor="#FFEE58"
                        inactiveDotColor="#90A4AE"

                    />
        </View>
      
      )
  }
}