import React, { Component } from 'react';
import {
  View,
  Text,
  Linking
} from 'react-native';
import styles from '../../../styles'
import WebView from 'react-native-webview'
import { widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Iframe from 'react-iframe'


export default class BePrepared extends Component{
  render(){
    return(
        <WebView style={styles.webView}
        source={{uri: 'https://youtu.be/bPITHEiFWLc'}}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={true}
        >
    </WebView>
    )
  }
}

