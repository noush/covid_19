import React,{Component} from 'react'
import {View,FlatList,Image,TouchableOpacity,Text,Alert,Linking} from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import style from '../../styles'
import {Actions} from 'react-native-router-flux'
export default class ShouldKnow extends Component{
    constructor() {
        super();
        this.state = {
            shouldDoList: [{
                text: 'About COVID-19'
            },
            {
                text: 'Be Prepared'
            },
            {
                text: 'Protect Yourself And Others'
            },
            {
                text: 'Myth Busters'
            },
        {
            text:'Stress Busters'
        }],
        info :[{
            text: 'Travel Advisory for People entering the country'
        },
        {
            text: 'Travel Recommendation for people travelling through affected countries '
        },
        {
            text: 'Frequently asked questions'
        },
        {
            text: 'Corona Virus Risk Areeas'
        },
    {
        text:'Videos & podcasts '
    }],
        }
    }
renderSeperator = ()=>{
    return(
        <View style ={{height:1,width:wp('100%'),backgroundColor:'#E5E7E9'}}>

        </View>
    )
}
actionOnRow(item, index) {
    const URL = "https://youtu.be/bPITHEiFWLc"
    //Alert.alert(index.toString());
    let rowIndexValue = index.toString()
    switch (rowIndexValue) {
        case '0': {Actions.about()}
            break;
        case '1': //Linking.openURL(URL).catch((err) => console.error('An error occurred', err));
        {Actions.bePrepared()}
            break;
        // case '2':  Actions.health()
        //     break;
        // case '3': Actions.physio()
        //     break;
        // case '4': Actions.labTest()
        //     break;
        default:
        
    }
}

    renderItem = ({ item,index }) => (

        <TouchableOpacity onPress={() =>
            this.actionOnRow(item, index)}
            style= {{height:hp('6%'),paddingTop:wp('1%'),marginLeft:wp('2%'),justifyContent:'center'}}>
            <Text style ={{color:'#2874A6',fontSize:wp('2.5%'),fontWeight:'bold'}}>{item.text}</Text>
        </TouchableOpacity>
   )
   keyExtractor = (item, index) => index.toString()
    render(){
        return(
            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
            <View style={{ width: wp('45%'), height: hp('48%'), backgroundColor: '#fff', marginTop: hp('2%') }}>
                <Text style={{fontSize:wp('3%'),fontWeight:'bold',padding:wp('2%')}}>What You Should Know</Text>
                <FlatList
                    data={this.state.shouldDoList}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem} 
                    ItemSeparatorComponent = {this.renderSeperator}/>
                     <View>
                    <Image
                           style={{width:wp('43%'),height:hp('12%'),marginLeft:wp('1%'),marginBottom:wp('1%')}}
                           source={require('../../../assets/mask.jpg')}/>
                    </View>
                           
            </View>
            <View style={{ width: wp('47%'), height: hp('48%'), backgroundColor: '#fff', marginTop: hp('2%') }}>
            <Text style={{fontSize:wp('3%'),fontWeight:'bold',padding:wp('2%')}}>Additional Information</Text>
            <FlatList
                    data={this.state.info}
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderItem} 
                    ItemSeparatorComponent = {this.renderSeperator}/>
                     <View>
                    <Image
                           style={{width:wp('45%'),height:hp('12%'),marginLeft:wp('1%'),marginBottom:wp('1%')}}
                           source={require('../../../assets/flight_new.jpg')}/>
                    </View>
            </View>
        </View>
        )
    }
}