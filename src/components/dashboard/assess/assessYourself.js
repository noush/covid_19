import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Alert,
  ScrollView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from '../../../styles';
import ImageComponent from '../../imageAsses/imageComponent'
import CheckBox from '@react-native-community/checkbox';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux';
import Colors from '../../colors'
const { width, height } = Dimensions.get('window');

const isSmallDevice = width <= 414;
const numColumns = isSmallDevice ? 3 : 4;



export default class AssessYourself extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pressedcloseContact :false,
      backgroundColorcloseContact: 'transparent',
      pressedTravelled :false,
      backgroundColorTravelled : 'transparent',
      backgroundColorTravelNo: 'transparent',
      pressedTravelledNo :false,
      backgroundColor: 'transparent',
      backgroundColorfever: 'transparent',
      backgroundColorShortness: 'transparent',
      backgroundColorFatigue: 'transparent',
      backgroundColorCough: 'transparent',
      rbackgroundColorHeadache: 'transparent',
      backgroundColorSputum: 'transparent',
      symptomsExist: [],
      pressed: false,
      pressedfever: false,
      pressedShortness: false,
      pressedFatigue: false,
      pressedCough: false,
      pressedHeadache: false,
      pressedSputum: false,
      checkStatus: false,
      finalSymptoms: [],
      slots:
        [{ "symptoms": "Fever " },
        { "symptoms": "Shortness of breath" },
        { "symptoms": "Fatigue " },
        { "symptoms": "Cough" },
        { "symptoms": "Headache" },
        { "symptoms": "Sputum Production" }
        ],


    }
  }

  contactCovidIllPerson = (res) => {
  let text = 'contacted with COVID 19 Positive person'
      if (!this.state.pressedcloseContact) {
  
        this.setState({ pressedcloseContact: true, backgroundColorcloseContact: Colors.Head });
        this.state.symptomsExist.push(text)
        console.log(this.state.symptomsExist)
      }
      else {
        this.setState({ pressedcloseContact: false, backgroundColorcloseContact: 'transparent' });
        let arr = this.state.symptomsExist.filter(symptom => symptom !== text)
        this.state.symptomsExist = []
        this.state.symptomsExist.push(...arr)
        console.log(this.state.symptomsExist)
      }
    }
  

  recentTravel = (res) => {
    let text = 'Travelled to COVID 19 effected Countries'
    if (!this.state.pressedcloseContact) {

      this.setState({ pressedcloseContact: true, backgroundColorTravelled: Colors.Head });
      this.state.symptomsExist.push(text)
      console.log(this.state.symptomsExist)
    }
    else {
      this.setState({ pressedcloseContact: false, backgroundColorTravelled: 'transparent' });
      let arr = this.state.symptomsExist.filter(symptom => symptom !== text)
      this.state.symptomsExist = []
      this.state.symptomsExist.push(...arr)
      console.log(this.state.symptomsExist)
    }
  }
  closeContactNo=() =>{
    if(!this.state.pressed){
      this.setState({ pressed: true,backgroundColor: Colors.Head});
   } else {
     this.setState({ pressed: false, backgroundColor: 'transparent' });
   }
 }
 travellNo =() =>{
  if(!this.state.pressedTravelledNo){
    this.setState({ pressedTravelledNo: true,backgroundColorTravelNo: Colors.Head});
 } else {
   this.setState({ pressedTravelledNo: false, backgroundColorTravelNo: 'transparent' });
 }
}
  symptmsCount = (res, index) => {
    this.state.symptomsExist.push(res)
    console.log(this.state.symptomsExist)
    if (index > 0) {
      this.setState({
        countOfSymptoms: true
      },
        () => {
          console.log(this.state.countOfSymptoms)
        })
    }

  }
  changeColorFever(symptomPassed) {
    if (!this.state.pressedfever) {

      this.setState({ pressedfever: true, backgroundColorfever: Colors.Head });
      this.state.symptomsExist.push(symptomPassed)
      console.log(this.state.symptomsExist)
    }
    else {
      this.setState({ pressedfever: false, backgroundColorfever: 'transparent' });
      let arr = this.state.symptomsExist.filter(symptom => symptom !== symptomPassed)
      this.state.symptomsExist = []
      this.state.symptomsExist.push(...arr)
      console.log(this.state.symptomsExist)
    }
  }

  changeColorShortness(symptomPassed) {
    if (!this.state.pressedShortness) {

      this.setState({ pressedShortness: true, backgroundColorShortness: Colors.Head });
      this.state.symptomsExist.push(symptomPassed)
      console.log(this.state.symptomsExist)
    }
    else {
      this.setState({ pressedShortness: false, backgroundColorShortness: 'transparent' });
      let arr = this.state.symptomsExist.filter(symptom => symptom !== symptomPassed)
      this.state.symptomsExist = []
      this.state.symptomsExist.push(...arr)
      console.log(this.state.symptomsExist)
    }
  }

  changeColorFatigue(symptomPassed) {
    if (!this.state.pressedFatigue) {

      this.setState({ pressedFatigue: true, backgroundColorFatigue: Colors.Head });
      this.state.symptomsExist.push(symptomPassed)
      console.log(this.state.symptomsExist)
    }
    else {
      this.setState({ pressedFatigue: false, backgroundColorFatigue: 'transparent' });
      let arr = this.state.symptomsExist.filter(symptom => symptom !== symptomPassed)
      this.state.symptomsExist = []
      this.state.symptomsExist.push(...arr)
      console.log(this.state.symptomsExist)
    }
  }

  changeColorCough(symptomPassed) {
    if (!this.state.pressedCough) {

      this.setState({ pressedCough: true, backgroundColorCough: Colors.Head });
      this.state.symptomsExist.push(symptomPassed)
      console.log(this.state.symptomsExist)
    }
    else {
      this.setState({ pressedCough: false, backgroundColorCough: 'transparent' });
      let arr = this.state.symptomsExist.filter(symptom => symptom !== symptomPassed)
      this.state.symptomsExist = []
      this.state.symptomsExist.push(...arr)
      console.log(this.state.symptomsExist)
    }
  }

  changeColorHeadache(symptomPassed) {
    if (!this.state.pressedHeadache) {

      this.setState({ pressedHeadache: true, backgroundColorHeadache: Colors.Head });
      this.state.symptomsExist.push(symptomPassed)
      console.log(this.state.symptomsExist)
    }
    else {
      this.setState({ pressedHeadache: false, backgroundColorHeadache: 'transparent' });
      let arr = this.state.symptomsExist.filter(symptom => symptom !== symptomPassed)
      this.state.symptomsExist = []
      this.state.symptomsExist.push(...arr)
      console.log(this.state.symptomsExist)
    }
  }

  changeColorSputum(symptomPassed) {
    if (!this.state.pressedSputum) {

      this.setState({ pressedSputum: true, backgroundColorSputum: Colors.Head });
      this.state.symptomsExist.push(symptomPassed)
      console.log(this.state.symptomsExist)
    }
    else {
      this.setState({ pressedSputum: false, backgroundColorSputum: 'transparent' });
      let arr = this.state.symptomsExist.filter(symptom => symptom !== symptomPassed)
      this.state.symptomsExist = []
      this.state.symptomsExist.push(...arr)
      console.log(this.state.symptomsExist)
    }
  }


  renderItem = ({ item, index }) => (


    <View  >
      <TouchableOpacity onPress={() => this.changeColor()} style={[styles.symptoms, { backgroundColor: this.state.backgroundColor }]}>
        <Text style={{ color: Colors.text, fontWeight: 'bold' }}>{item.symptoms}</Text>
      </TouchableOpacity>
    </View>


  )


  keyExtractor = (item, index) => index.toString()

  submit = () => {
    Actions.info()
  }
  render() {

    return (
      <View style={styles.container}>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <Text style={styles.Heading}>Assess Yourself</Text>
        </View>
        <View style={{ flex: 1, backgroundColor: Colors.innerFlex }}>


          <Text style={styles.symptm}>Select all symptoms that you have</Text>
          {/* <View>
          <FlatList
            data={this.state.slots}
            renderItem={this.renderItem}
            numColumns={numColumns} 
            keyExtractor={this.keyExtractor} />
        </View> */}

          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>

            <TouchableOpacity onPress={() => this.changeColorFever('fever')} style={[styles.symptoms, { backgroundColor: this.state.backgroundColorfever }]}>
              <Text style={{ color: Colors.text, fontWeight: 'bold' }}>Fever</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.changeColorShortness('Shortness')} style={[styles.symptoms, { backgroundColor: this.state.backgroundColorShortness }]}>
              <Text style={{ color: Colors.text, fontWeight: 'bold' }}>Shortness of breath</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.changeColorFatigue('Fatigue')} style={[styles.symptoms, { backgroundColor: this.state.backgroundColorFatigue }]}>
              <Text style={{ color: Colors.text, fontWeight: 'bold' }}>Fatigue</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.changeColorCough('Cough')} style={[styles.symptoms, { backgroundColor: this.state.backgroundColorCough }]}>
              <Text style={{ color: Colors.text, fontWeight: 'bold' }}>Cough</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.changeColorHeadache('Headache')} style={[styles.symptoms, { backgroundColor: this.state.backgroundColorHeadache }]}>
              <Text style={{ color: Colors.text, fontWeight: 'bold' }}>Headache</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.changeColorSputum('Sputum')} style={[styles.symptoms, { backgroundColor: this.state.backgroundColorSputum }]}>
              <Text style={{ color: Colors.text, fontWeight: 'bold' }}>Sputum Production</Text>
            </TouchableOpacity>
          </View>


          <View>
            <Text style={styles.question}>Have you come in close contact with a person known to have COVID-19 illness in last 14 days ? </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View>
              <TouchableOpacity onPress={() => this.contactCovidIllPerson('Yes')}
              style={[styles.symptomChk,{ backgroundColor: this.state.backgroundColorcloseContact }]}>
                <Text style={styles.text}>Yes</Text>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity onPress={() => this.closeContactNo()}
              style={[styles.symptomChk,{ backgroundColor: this.state.backgroundColor }]}>
                <Text style={styles.text}>No</Text>
              </TouchableOpacity>
            </View>
          </View>



          <View>
            <Text style={styles.question}>Have you recently in last 1 month travelled to Corona Virus (COVID-19) affected countries ? </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View>
              <TouchableOpacity onPress={() => this.recentTravel('Yes')}
              style={[styles.symptomChk,{ backgroundColor: this.state.backgroundColorTravelled }]}>
                <Text style={styles.text}>Yes</Text>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity onPress={() => this.travellNo()}
                style={[styles.symptomChk,{ backgroundColor: this.state.backgroundColorTravelNo }]}>
                <Text style={styles.text}>No</Text>
              </TouchableOpacity>
            </View>
          </View>


          <View style={styles.submt} >
            <TouchableOpacity onPress={() => this.submit()}
            >
              <Text style={styles.bttn} >Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

    )
  }
}
