import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
  Alert
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from '../../../styles';
import ImageComponent from '../../imageAsses/imageComponent'
import CheckBox from 'react-native-modest-checkbox';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class AssessYourself extends Component{
    constructor(props) {
        super(props);
        this.state = {
            precautions:
                [{ "measures": "First contact your family physician by phone ot ask for advice by calling the family physician advisory line 1220 or call ambulance 112. Inform your doctor and ambulance about your recent travel in the area with ongoing coronavirus transmission. They will give you instructions on how to proceed." },
                { "measures": "Stay indoors and avoid contact with other people" },
                { "measures": "Cover your mouth and nose with a tissue when you cough or sneeze. Dispose of the tissue immediately after use. " },
                { "measures": "If you do not have a tissue, use your sleeve(part of your forearm) and not your bare hand to cover your mouth" },
                { "measures": "When you arrive at a health care facility, you may be given a mask. This is to protect healthcare professionals and other people." }
                ],

         
        }
    }


    renderItem= ({item})=>(
        <View style = {{flex:1,flexDirection:'row',marginLeft:wp('5%'),marginTop:hp('2%'),marginRight:wp('4%')}}>
        <Icon name='cards-diamond' size={15} color='#17CAE8'/>
    <Text style={{marginLeft:wp('2%'),fontWeight:'bold',fontSize:hp('1.6%')}}>{item.measures}</Text>
        </View> 
    )
         
    keyExtractor = (item, index) => index.toString()  

  render(){
  
    return(
 <View style = {styles.container}>
<ImageComponent/>


<View style = {{marginLeft:wp('2%'),marginTop:hp('3%')}}>
    <Text style={{marginLeft:wp('2%'),fontWeight:'bold',fontSize:hp('2%')}}>Continue to follow the precautionary measures as suggested by Health Administrator</Text>
    </View> 

<FlatList
                    data={this.state.precautions}
                    renderItem={this.renderItem}
                    //numColumns={numColumns} 
                    keyExtractor={this.keyExtractor}/>
 </View>
 
      )
  }
}
