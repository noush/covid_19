import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Alert,
  TextInput
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from '../../../styles';
import ImageComponent from '../../imageAsses/imageComponent'
import CheckBox from 'react-native-modest-checkbox';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux';
import { TextField } from 'react-native-material-textfield';
import { RadioButton } from 'react-native-paper';


export default class AssessYourself extends Component {

  constructor(props) {
    super(props);
    this.state = {
      checkStatus: false,
      checked: 'enable',
      inputLabel:
        [{ "label": "Cell Number " },
        { "label": "Home Number" },
        { "label": "Alternative Contact Number " }
        ],
        addr :[
          { "label": "Address 1 " },
          { "label": "Address 2 " },
          { "label": "Zip code " }
          ],


    }
  }

  submit = () => {
    Actions.precautions();
  }

  handlePhoneNumberChange = () => {
    Alert.alert('rej;fk;ofk')
  }


  renderItem = ({ item }) => (
<View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: hp('2%') }}>
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
        <Text style ={{ width:wp('20%')}} >{item.label}</Text>
      </View>
      <View style={{ width: wp('70%') }}>
        <TextInput
          style={styles.txtInput}
          maxLength={20}
          keyboardType={"phone-pad"}
          //value={this.state.name}
          onChangeText={this.handlePhoneNumberChange}
        />
      </View>
    </View>
  )

  keyExtractor = (item, index) => index.toString()

  render() {

    return (
      <View style={styles.container}>
        <ScrollView vertical={true} showsVerticalScrollIndicator={false}>
          <View style={{ flex: 1 }}>
            <ImageComponent />
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: hp('3%') }}>
            <Text style={{ fontSize: wp('5%'), color: '#17CAE8', fontWeight: 'bold' }}>Contact Information</Text>
          </View>


          <View style={{ marginLeft: wp('3%'), marginTop: hp('2%'), marginRight: wp('4%') }}>

            <Text style={{ fontWeight: 'bold', fontSize: hp('2%') }}>Share Following information. Health Administrator will reach out you</Text>
          </View>

          <View>
            <FlatList
              data={this.state.inputLabel}
              renderItem={this.renderItem}
              //numColumns={numColumns} 
              keyExtractor={this.keyExtractor} />
          </View>


<View style = {{marginTop:hp('3%'),flexDirection:'row'}}>
  <View>
  <RadioButton
          value="enable"
          status={checked === 'enable' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ checked: 'disable' }); }}
        />
  </View>
  <View>
    <Text>Geo Tag</Text>
  </View>

  <View>
            <FlatList
              data={this.state.addr}
              renderItem={this.renderItem}
              //numColumns={numColumns} 
              keyExtractor={this.keyExtractor} />
          </View>

</View>
          <View style={{ flex: 1, justifyContent: 'flex-end' }} >
            <TouchableOpacity
              style={styles.submt_info}
              onPress={() => this.submit()}>
              <Text>Submit</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
      </View>

    )
  }
}
