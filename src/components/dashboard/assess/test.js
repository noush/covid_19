import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button,TouchableOpacity} from 'react-native';

export default class App extends Component {
  state={
    backgroundColor: 'black',
    backgroundColor2: 'black',
    pressed: false,
  };

  changeColor(){
    if(!this.state.pressed){
       this.setState({ pressed: true,backgroundColor: 'red', backgroundColor2: 'black'});
    } else {
      this.setState({ pressed: false, backgroundColor: 'black' ,backgroundColor2: 'red'});
    }
  }
  render() {
    return (
      <View style={styles.container}>
          <TouchableOpacity
              style={{backgroundColor:this.state.backgroundColor, padding: 15}}
              onPress={()=>this.changeColor()}
                >
            <Text style={styles.text}>1 Button</Text>
          </TouchableOpacity>

          <TouchableOpacity
              style={{backgroundColor:this.state.backgroundColor2, padding: 15}}
              onPress={()=>this.changeColor()}
                >
            <Text style={styles.text}>2 button!</Text>
          </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  text:{
    color:'white'
    },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});