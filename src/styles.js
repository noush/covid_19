import { StyleSheet, Dimensions } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Colors from '../src/components/colors'

export default StyleSheet.create({

    container: {
        flex: 1,
        padding: wp('2%'),
        backgroundColor: Colors.theme
    },
    mainPage:{flex: 1,
            backgroundColor: Colors.theme  
    },
    innercontainer:{
        padding: wp('2%'),
        backgroundColor: Colors.theme
    },
    imageView:
    {
        width: wp('96%'),
        height: hp('15%'),
        marginTop: hp('2%'),
    },
    fitImage: {
        borderRadius: wp('50%'),
    },
    risk: {
        alignItems: 'flex-end',
        fontSize: hp('2.7%'),
        paddingTop: wp('3%'),
        paddingRight: hp('4%'),
        color: '#fff',
        fontWeight: 'bold'
    },
    assess: {
        justifyContent: 'flex-end',
        fontSize: hp('2%'),
        paddingTop: wp('5%'),
        paddingRight: hp('10%'),
        color: '#fff',
        fontWeight: 'bold'
    },
    imageTextView: {
        width: wp('96%'),
        height: hp('15%'),
        position: 'absolute',
        alignItems: 'flex-end'
    },
    dashboard: {
        width: wp('96%'),
        height: hp('40%'),
        marginTop: hp('2%'),
        backgroundColor: 'red'

    },
    dashboardInner: {
        width: wp('45%'),
        height: hp('40%'),
        marginTop: hp('-20%'),
        backgroundColor: '#f9f9f9',
        elevation: 4,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        margin: wp('1%'),
    },
    symptoms:
    {
        padding: wp('2%'),
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: Colors.Head,
        margin: wp('2%'),
        marginLeft: wp('3%'),
        borderRadius: wp('50%')
    },
    submt: {
        width: wp('90%'),
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderColor: Colors.Head,
        marginTop: wp('5%'),
        borderRadius: wp('50%'),
        height: hp('5%'),
        marginLeft: wp('3%'),
        marginRight: wp('5%'),
        backgroundColor: Colors.Head
    },
    submt_info: {
        width: wp('90%'),
        borderWidth: 2,
        borderColor: '#3BAD87',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: wp('50%'),
        height: hp('5%'),
        marginLeft: wp('3%'),
        marginRight: wp('5%'),
        backgroundColor: '#3BAD87'
    },
    txtInput:
    {
        height: hp('5%'),
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: wp('3%')
    },
    map: {
        flex: 1
      },
      mapMarkerContainer: {
        left: '47%',
        position: 'absolute',
        top: '42%'
      },
      mapMarker: {
        fontSize: 40,
        color: "red"
      },
      deatilSection: {
        flex: 1,
        backgroundColor: "#fff",
        padding: 10,
        display: "flex",
        justifyContent: "flex-start"
      },
      spinnerView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
      },
      btnContainer: {
        width: Dimensions.get("window").width - 20,
        position: "absolute",
        bottom: 100,
        left: 10
      },
      headder:{
          backgroundColor:Colors.theme
      },
      text:{
          color:Colors.text,
          fontWeight:'bold'
      },
      bttn:{
        
            color:Colors.text,
            fontWeight:'bold',
            fontSize:hp('2%'),
            fontFamily:'sans-serif-condensed'
        },
    question:{
        fontSize: wp('3%'), marginBottom: hp('2%'), fontWeight: 'bold', marginTop: hp('2%'), marginLeft: wp('2%'),color:Colors.text 
    },
    symptm:
        { fontSize: wp('4%'),
         marginTop: hp('2%'),
          marginBottom: hp('2%'), 
          fontWeight: 'bold',
           marginLeft: wp('2%'),
           color:Colors.text 
        },
        Heading:
        {
            color:Colors.text,
            fontSize:hp('3%'),
            fontWeight:'bold',
            marginTop:hp('4%'),
            marginLeft:hp('4%'),
        },
        subheading:
        {
            color:Colors.text,
            fontSize:hp('1.5%'),
            fontWeight:'bold',
            marginTop:hp('1%'),
            marginLeft:hp('4%'),
            marginBottom:hp('3%'),
        },
        submain:
        {
            color:Colors.text,
            fontSize:hp('2%'),
            fontWeight:'bold',
            marginTop:hp('2%'),
            marginLeft:hp('2%'),
            marginBottom:hp('2%'),
        },
        symptomChk:
        { width: wp('20%'),
         justifyContent: 'space-evenly', 
        alignItems: 'center', 
        borderWidth: 2, 
        borderColor: Colors.Head,
         margin: wp('2%'), 
        marginLeft: wp('3%'), borderRadius: wp('50%'), 
        height: hp('5%') 
    }
    
      
})