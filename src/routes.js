import React from 'react'
import { Actions,Router, Scene,ActionConst } from 'react-native-router-flux'
import Colors from './components/colors'
import Home from './home'
import MainPage from './components/mainPage'
import AssessPage from './components/imageAsses/assess' 
import AssessNo from './components/imageAsses/assessNo'
import Conatact from './components/contact/contactList'
import AboutCovid from './components/ShouldKnow/shouldKnwComponent/about'
import BePrepared from './components/ShouldKnow/shouldKnwComponent/bePrepared'
import AssessYourself from './components/dashboard/assess/assessYourself'
import Information from './components/dashboard/assess/info'
import Precautions from './components/dashboard/assess/precaution'
import GooglePlacesApi from './components/places'
import MapTest from './components/additnlInfo/manualMap'
import LiveMap from './components/additnlInfo/liveMap'
import BottomView from './components/customHeader/bottom'
import ContactTracing from './components/contactTracker'
import Test from './components/dashboard/assess/test'

const scenes = Actions.create(
   
        <Scene key="root">
           <Scene key="home" component={MainPage}  initial={true} hideNavBar={true} />
           <Scene key="assessYourself" component={AssessYourself} leftButtonIconStyle={{color:'white',backgroundColor:'white'}} navigationBarStyle={{backgroundColor:Colors.theme }}  barButtonIconStyle={{tintColor: 'white'}} />
           <Scene key="info" component={Information} hideNavBar={true} />
           <Scene key="precautions" component={Precautions} hideNavBar={true} />
           <Scene key="places" component={GooglePlacesApi} hideNavBar={true} />
           <Scene key="testMap" component={MapTest} navigationBarStyle={{backgroundColor:Colors.theme }}  barButtonIconStyle={{tintColor: 'white'}}/>
           <Scene key="liveMap" component={LiveMap} hideNavBar={true} />
           <Scene key="bottom" component={BottomView} hideNavBar={true} />
           <Scene key="test" component={Test} hideNavBar={true} />
           <Scene key="contactTracing" component={ContactTracing} navigationBarStyle={{backgroundColor:Colors.theme }}  barButtonIconStyle={{tintColor: 'white'}} />
            {/* <Scene key="home" component={Home}  initial={true} hideNavBar={true} /> */}
            <Scene key="assess" component={AssessPage} hideNavBar={true} />
            <Scene key="symptomNo" component={AssessNo} hideNavBar={true} />
            <Scene key="contact" component={Conatact} hideNavBar={true} />
            <Scene key="about" component={AboutCovid} hideNavBar={true} />
            <Scene key="bePrepared" component={BePrepared} hideNavBar={true} />
        </Scene>

)
export default class RoutePath extends React.Component {

    render () {
      return <Router scenes={scenes} tintColor='white' barButtonIconStyle={{tintColor: 'white'}}/>
    }
  }