import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    FlatList,
    TouchableOpacity,
    Alert
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import styles from './styles'
import FitImage from 'react-native-fit-image'
import ImageComponent from '../src/components/imageAsses/imageComponent'
import ListOFTask from '../src/components/ShouldKnow/shouldknow'
export default class Home extends Component {
 

    render() {
        return (
            <View style={styles.container}>

              <ImageComponent/>

               <ListOFTask/>


            </View>
        )
    }
}


