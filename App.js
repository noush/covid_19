/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Platform
} from 'react-native';
import Routes from './src/routes'
import BottonView from './src/components/customHeader/bottom'

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 22;
const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);
export default class App extends Component {
  
  render() {
    console.log(StatusBar.currentHeight)
    return (
      <View style={styles.container}>
        <MyStatusBar backgroundColor="#335e90" barStyle="light-content" />
        
      
        <Routes />
        <BottonView/>
      </View>

    )
  }
}


const styles = StyleSheet.create({

  statusBar: {
    height: STATUSBAR_HEIGHT,
  },

  engine: {
    position: 'absolute',
    right: 0,
  },
  container: {
    flex: 1
  },

  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

